FROM openjdk:17-jdk-alpine
COPY ["./target/testexercise-0.0.1-SNAPSHOT.jar", "./"]
CMD ["java","-jar","./testexercise-0.0.1-SNAPSHOT.jar"]