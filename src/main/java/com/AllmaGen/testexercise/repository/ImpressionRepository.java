package com.AllmaGen.testexercise.repository;

import com.AllmaGen.testexercise.model.data.Impression;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ImpressionRepository extends ElasticsearchRepository<Impression,String> {
}
