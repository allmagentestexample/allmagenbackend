package com.AllmaGen.testexercise.repository;

import com.AllmaGen.testexercise.model.data.Event;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface EventRepository extends ElasticsearchRepository<Event,String> {
}
