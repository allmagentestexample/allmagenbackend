package com.AllmaGen.testexercise.service;

import com.AllmaGen.testexercise.model.DiagramAxes;
import com.AllmaGen.testexercise.model.agregationTable.AggregationRequest;
import com.AllmaGen.testexercise.model.agregationTable.AggregationTableResponse;
import com.AllmaGen.testexercise.model.agregationTable.AggregationTableEntity;
import com.AllmaGen.testexercise.model.ctr.DiagramRequest;
import com.AllmaGen.testexercise.model.ctr.AxisResponse;
import com.AllmaGen.testexercise.model.statistic.CountResponse;
import com.AllmaGen.testexercise.model.statistic.EventRecord;
import com.AllmaGen.testexercise.model.statistic.EventsListRecord;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Predicate;

@Service
@RequiredArgsConstructor
public class StatisticService {
    private final static String FIELD_CRT = "fclick";
    private final static String EVENT_PREFIX = "v";

    private final static Set<String> EVPM_EXCLUDE = new HashSet<>(Collections.singletonList(FIELD_CRT));

    private final ElasticSearchService elasticSearchService;
    private final ElasticApiClient elasticApiClient;


    public CountResponse getCountResponse() {
        return new CountResponse(elasticSearchService.getEventCount(), elasticSearchService.getImpressionCount());
    }

    public EventsListRecord getEventListRecord() {
        EventsListRecord eventsListRecord = elasticApiClient.getEventListRecord();
        long totalEvent = elasticSearchService.getEventCount();
        eventsListRecord.getEventRecords().forEach(s -> {
            s.setPercent(calcPercent(totalEvent, s.getCount()));
        });
        return eventsListRecord;
    }

    public AxisResponse getCtr(DiagramRequest ctr) {
        String allData = "all";
        DiagramAxes diagramAxes = new DiagramAxes();
        elasticApiClient.getCtrStatistic(ctr, diagramAxes, true, allData);
        ctr.setEvents(Collections.singletonList(FIELD_CRT));
        elasticApiClient.getCtrStatistic(ctr, diagramAxes, false, FIELD_CRT);
        // Рассчитываем для событий fClick
        List<Object> yValues = new ArrayList<>(diagramAxes.getXAxis().size());
        diagramAxes.getXAxis().forEach(date -> {
            if (diagramAxes.getValuesMap().get(date).containsKey(FIELD_CRT)) {
                double fclick = diagramAxes.getValuesMap().get(date).get(FIELD_CRT);
                double all = diagramAxes.getValuesMap().get(date).get(allData);
                yValues.add(fclick / all * 100.0);
            } else {
                yValues.add(0);
            }
        });
        AxisResponse axisResponse = new AxisResponse(diagramAxes.getXAxis());
        axisResponse.addYaxis("CRT", yValues);
        return axisResponse;
    }

    public AxisResponse getEvPm(DiagramRequest diagramRequest) {
        String allData = "all";
        DiagramAxes diagramAxes = new DiagramAxes();
        elasticApiClient.getCtrStatistic(diagramRequest, diagramAxes, true, allData);
        AxisResponse axisResponse = new AxisResponse(diagramAxes.getXAxis());
        diagramRequest.getEvents().forEach(event -> {
            DiagramRequest d1 = new DiagramRequest();
            d1.setPeriod(diagramRequest.getPeriod());
            d1.setEvents(Collections.singletonList(event));
            elasticApiClient.getCtrStatistic(d1, diagramAxes, false, event);


            List<Object> yValues = new ArrayList<>(diagramAxes.getXAxis().size());
            diagramAxes.getXAxis().forEach(date -> {
                if (diagramAxes.getValuesMap().get(date).containsKey(event)) {
                    double evCount = diagramAxes.getValuesMap().get(date).get(event);
                    double all = diagramAxes.getValuesMap().get(date).get(allData);
                    yValues.add(1000.0 * evCount / all);
                } else {
                    yValues.add(0);
                }
            });
            axisResponse.addYaxis(event, yValues);
        });
        return axisResponse;
    }

    public AggregationTableResponse getTable(AggregationRequest param){

        List<AggregationTableEntity> list = elasticApiClient.getAggregationTable(param);
        AggregationTableResponse response = new AggregationTableResponse(list.size());
        list.forEach(s-> {
            double ctr = 100.0 * s.getEvents().getOrDefault(FIELD_CRT, 0L) / s.getCountImpression();
            response.addRow(s.getName(), s.getCountImpression(), ctr, calculateEvPmMap(s.getEvents(), s.getCountImpression()));
        });
        response.setEventList(getEventListRecord()
                .getEventRecords().stream()
                .map(EventRecord::getEvent)
                .filter(getEventEvPm())
                .toList());
        return response;
    }

    /**
     * Вычисление процентного соотношения события
     *
     * @param totalCount общее количество события
     * @param eventCount количество события
     * @return процентное соотношение
     */
    private double calcPercent(long totalCount, long eventCount) {
        return (double) eventCount / (double) totalCount * 100;
    }

    private Map<String, Double> calculateEvPmMap(Map<String, Long> param, long impressionCount){
        Map<String, Double> map = new HashMap<>();
        param.keySet()
                .stream()
                .filter(s-> (getEventEvPm().test(s) || (s.startsWith(EVENT_PREFIX)&& !param.containsKey(s.substring(1)))))
                .forEach(ev -> {
                    if (ev.startsWith(EVENT_PREFIX))
                        ev = ev.substring(1);
                    map.put(ev, calculateEvPm(param, ev, impressionCount));
                });
        return map;
    }

    private Double calculateEvPm(Map<String, Long> param, String event, long impressionCount){
        return 1000.0 * (param.getOrDefault(event, 0L) +
                param.getOrDefault(EVENT_PREFIX + event, 0L) )/ impressionCount;
    }

    private Predicate<String> getEventEvPm(){
        return (event) -> {
            return !event.startsWith(EVENT_PREFIX) &&
                    !EVPM_EXCLUDE.contains(event);
        };
    }


}
