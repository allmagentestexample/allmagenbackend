package com.AllmaGen.testexercise.service;

import com.AllmaGen.testexercise.model.data.DataIndex;
import com.AllmaGen.testexercise.repository.EventRepository;
import com.AllmaGen.testexercise.repository.ImpressionRepository;
import com.AllmaGen.testexercise.service.parse.Parser;
import com.AllmaGen.testexercise.service.parse.ParserRepositoryFactory;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ElasticSearchService {
    private final Logger log = LogManager.getLogger(this.getClass());
    private static final int MAX_COUNT_REQUEST_BULK = 10_000;

    private final EventRepository eventRepository;
    private final ImpressionRepository impressionRepository;
    private final ParserRepositoryFactory parserRepositoryFactory;

    /**
     * Импорт файла с данными события
     *
     * @param file полученный файл
     */
    public void addData(MultipartFile file) {
        try {
            InputStream inputStream = file.getInputStream();

            List<DataIndex> data = new ArrayList<>((int) (1_000 * 1.5));
            List<String> lines = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).lines().toList();
            Parser parser = parserRepositoryFactory.getParser(lines.get(0));
            lines.stream().skip(1)
                    .forEach(line -> {
                        try {
                            data.add(parser.parseLine(line));
                            if (data.size() >= MAX_COUNT_REQUEST_BULK) {
                                parser.getRepository().saveAll(data);
                                data.clear();
                            }
                        } catch (Exception e) {
                            log.info("Ошибка при парсинге строки {}, строка не была добавлена}", line);
                        }
                    });
            parser.getRepository().saveAll(data);
        } catch (IOException e) {
            log.info("Ошибка при чтении полученных данных");
            throw new RuntimeException(e);
        } catch (Exception e) {
            log.info(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public Map<String, Long> getCount(List<String> ids) {
        Map<String, Long> map = new HashMap<>();
        eventRepository.findAllById(ids).forEach(s -> {
            Long l = map.getOrDefault(s.getEvent(), 0L);
            map.put(s.getEvent(), ++l);
        });
        return map;
    }

    public long getEventCount() {
        return eventRepository.count();
    }

    public long getImpressionCount() {
        return impressionRepository.count();
    }

}
