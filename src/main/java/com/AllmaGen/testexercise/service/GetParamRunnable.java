package com.AllmaGen.testexercise.service;

import co.elastic.clients.elasticsearch._types.FieldValue;
import com.AllmaGen.testexercise.model.agregationTable.AggregationTableEntity;
import com.AllmaGen.testexercise.model.data.constant.ImpressionIndexHelper;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

@RequiredArgsConstructor
public class GetParamRunnable implements Callable<Boolean> {
    private final ElasticRequestComponent elasticRequestComponent;
    private final ElasticSearchService elasticSearchService;
    private final String fieldName;
    private final AggregationTableEntity value;

    @Override
    public Boolean call() throws Exception {
        List<String> uidList = elasticRequestComponent.getUidList(ImpressionIndexHelper.INDEX_NAME, fieldName,
                Collections.singletonList(FieldValue.of(value.getName())), value.getCountImpression());
        value.setEvents(elasticSearchService.getCount(uidList));
        return true;
    }
}
