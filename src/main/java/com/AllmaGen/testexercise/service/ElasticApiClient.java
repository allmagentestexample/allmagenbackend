package com.AllmaGen.testexercise.service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldValue;
import co.elastic.clients.elasticsearch._types.aggregations.CalendarInterval;
import co.elastic.clients.elasticsearch._types.aggregations.DateHistogramBucket;
import co.elastic.clients.elasticsearch._types.aggregations.StringTermsBucket;
import co.elastic.clients.elasticsearch._types.query_dsl.TermsQuery;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import com.AllmaGen.testexercise.model.DiagramAxes;
import com.AllmaGen.testexercise.model.PeriodEnum;
import com.AllmaGen.testexercise.model.agregationTable.AggregationRequest;
import com.AllmaGen.testexercise.model.agregationTable.AggregationTableEntity;
import com.AllmaGen.testexercise.model.data.constant.EventIndexHelper;
import com.AllmaGen.testexercise.model.data.constant.ImpressionIndexHelper;
import com.AllmaGen.testexercise.model.ctr.DiagramRequest;
import com.AllmaGen.testexercise.model.statistic.EventRecord;
import com.AllmaGen.testexercise.model.statistic.EventsListRecord;
import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.client.RestClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;


@Service
public class ElasticApiClient {
    private final Logger log = LogManager.getLogger(this.getClass());
    private final static String EVENT_INDEX_NAME = "event";
    private final static String EVENT_FIELD_NAME = "event_title";

    private final static int REQUEST_PART_SIZE = 10_000;

    private final ElasticRequestComponent elasticRequestComponent;
    private final ElasticsearchClient client;
    private final ElasticSearchService elasticSearchService;

    private final ExecutorService executorService = Executors.newFixedThreadPool(8);

    public ElasticApiClient(@Value("${spring.data.elasticsearch.cluster-nodes}") String elasticHost,
                            ElasticRequestComponent elasticRequestComponent, ElasticSearchService elasticSearchService) {
        this.elasticRequestComponent = elasticRequestComponent;
        this.elasticSearchService = elasticSearchService;
        // Create the low-level client
        String host = elasticHost.substring(0, elasticHost.indexOf(":"));
        int port = Integer.parseInt(elasticHost.substring(elasticHost.indexOf(":") + 1));
        RestClient restClient = RestClient.builder(new HttpHost(host, port)).build();

        // Create the transport with a Jackson mapper
        ElasticsearchTransport transport = new RestClientTransport(restClient, new JacksonJsonpMapper());

        // And create the API client
        client = new ElasticsearchClient(transport);
    }

    /**
     * Получение списка событий и их количества
     * @return
     */
    public EventsListRecord getEventListRecord() {
        String aggName = EventIndexHelper.FIELD_EVENT;
        SearchRequest searchRequest = elasticRequestComponent.getSearchAggregationRequest(EventIndexHelper.INDEX_NAME,
                Collections.singletonList(aggName));
        try {
            SearchResponse<Void> search = client.search(searchRequest, Void.class);
            List<StringTermsBucket> buckets = search.aggregations().get(aggName).sterms().buckets().array();
            EventsListRecord result = new EventsListRecord(buckets.size());
            buckets.forEach(b -> result.addEvent(new EventRecord(b.key().stringValue(), b.docCount())));
            return result;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void getCtrStatistic(DiagramRequest diagramRequest, DiagramAxes diagramAxes, boolean isAllEvents, String dName) {

        TermsQuery termsQuery = null;
        if (!isAllEvents && diagramRequest.getEvents() != null && diagramRequest.getEvents().size() > 0) {
            List<String> uidList = elasticRequestComponent.getUidList(EVENT_INDEX_NAME, EVENT_FIELD_NAME,
                    convertListStringToListFieldValue(diagramRequest.getEvents(), true), 0L);
            termsQuery = elasticRequestComponent.getTermQuery(ImpressionIndexHelper.FIELD_UID, uidList.stream().map(FieldValue::of).toList());
        }
        getDiagramAxesFromHistogramDataRequest(ImpressionIndexHelper.INDEX_NAME, termsQuery, diagramAxes,
                getCalendarInterval(diagramRequest.getPeriod()), isAllEvents, dName);
    }

    public List<AggregationTableEntity> getAggregationTable(AggregationRequest param){


        String fieldName = param.getParamName();

        //1. Получаем все агрегацию по mm_dma и общее количество показов
        SearchRequest searchRequest = elasticRequestComponent.getSearchAggregationRequest(ImpressionIndexHelper.INDEX_NAME,
                Collections.singletonList(fieldName));
        try {
            SearchResponse<Void> search = client.search(searchRequest, Void.class);
            List<StringTermsBucket> buckets = search.aggregations().get(fieldName).sterms().buckets().array();
            List<AggregationTableEntity> result = new ArrayList<>(buckets.size());
            buckets.forEach(b -> result.add(new AggregationTableEntity(b.key().stringValue(), b.docCount())));
             List<Callable<Boolean>> callables = result
                    .stream().map(s->new GetParamRunnable(elasticRequestComponent,
                            elasticSearchService, fieldName, s)).collect(Collectors.toList());

            try {
                List<Future<Boolean>> future = executorService.invokeAll(callables);
                future.forEach(f -> {
                    try {
                        f.get();
                    } catch (InterruptedException | ExecutionException e) {
                        log.error("Ошибка при ожидании ответа от эластика");
                    }
                });
            } catch (InterruptedException e) {
                log.error("Ошибка при ожидании ответа от эластика");
                throw new RuntimeException(e);
            }


            return result;
        } catch (IOException | RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Составление агрегационного запроса по дате на основе DateHistogram
     *
     * @param index            имя индекса
     * @param termsQuery       условие выборки
     * @param calendarInterval интервал агрегации
     * @return значение осей
     */
    private DiagramAxes getDiagramAxesFromHistogramDataRequest(String index, TermsQuery termsQuery, DiagramAxes diagramAxes,
                                                               CalendarInterval calendarInterval, boolean isAllEvent, String dName) {
        String diagramName ="agg";
        SearchRequest.Builder builder = new SearchRequest.Builder()
                .index(index)
                .size(0);
        if (termsQuery != null && !isAllEvent) {
            builder.query(termsQuery._toQuery());
        }

        SearchRequest searchRequest = builder
                .aggregations(diagramName, a -> a.dateHistogram(h -> h
                        .field(ImpressionIndexHelper.FIELD_DATA)
                        .calendarInterval(calendarInterval)))
                .build();

        try {
            SearchResponse<Void> search = client.search(searchRequest, Void.class);
            List<DateHistogramBucket> bucket = search.aggregations().get(diagramName).dateHistogram().buckets().array();

            if (isAllEvent) {
                diagramAxes.setSize(bucket.size());
            }
            bucket.forEach(s -> {
                String x = convertTimeStampToStringForDaily(s.key());
                Long y = s.docCount();
                if (isAllEvent){
                    diagramAxes.axisValueMap(x, y);
                }
                diagramAxes.putValueMap(x, dName, y);
            });
            return diagramAxes;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }




    private String convertTimeStampToStringForDaily(String time) {
        long timestamp = Long.parseLong(time);
        Date date = new Date(timestamp);
        Format format = new SimpleDateFormat("dd.MM.yy");
        return format.format(date);

    }

    private CalendarInterval getCalendarInterval(PeriodEnum periodEnum) {
        return switch (periodEnum) {
            case DAILY -> CalendarInterval.Day;
            case WEEK -> CalendarInterval.Week;
            case MONTH -> CalendarInterval.Month;
        };
    }

    /**
     * Конвертация значений событий в значения полей для поиска. Добавляет события с префиксом v
     *
     * @param value       коллекция значений событий
     * @param joinVEvents флаг необходимости добавлять события "v"
     * @return коллекция значений полей для поиска
     */
    private List<FieldValue> convertListStringToListFieldValue(List<String> value, boolean joinVEvents) {
        List<FieldValue> result = new ArrayList<>();
        value.forEach(s -> {
            if (joinVEvents && !s.startsWith("v")) {
                result.add(FieldValue.of("v" + s));
            }
            result.add(FieldValue.of(s));
        });
        return result;
    }

}
