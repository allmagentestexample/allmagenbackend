package com.AllmaGen.testexercise.service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.FieldValue;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregation;
import co.elastic.clients.elasticsearch._types.query_dsl.TermsQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.TermsQueryField;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.client.RestClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ElasticRequestComponent {
    private final Logger log = LogManager.getLogger(this.getClass());
    private final static int REQUEST_PART_SIZE = 10_000;
    private final ElasticsearchClient client;

    public ElasticRequestComponent(@Value("${spring.data.elasticsearch.cluster-nodes}") String elasticHost) {
        // Create the low-level client
        String host = elasticHost.substring(0, elasticHost.indexOf(":"));
        int port = Integer.parseInt(elasticHost.substring(elasticHost.indexOf(":") + 1));
        RestClient restClient = RestClient.builder(new HttpHost(host, port)).build();

        // Create the transport with a Jackson mapper
        ElasticsearchTransport transport = new RestClientTransport(restClient, new JacksonJsonpMapper());

        // And create the API client
        client = new ElasticsearchClient(transport);
    }

    public SearchRequest getSearchAggregationRequest(String indexName, List<String> aggregationList) {
        return new SearchRequest.Builder()
                .index(indexName)
                .size(0)
                .aggregations(getMapAggregations(aggregationList))
                .build();
    }


    public Map<String, Aggregation> getMapAggregations(List<String> fields){
        Map<String, Aggregation> result = new HashMap<>(fields.size());
        fields.forEach(s-> result.put(s, createAggregation(s)));
        return result;
    }

    public Aggregation createAggregation(String field){
        return Aggregation.of(a -> a.terms(h -> h.field(field).size(REQUEST_PART_SIZE)));
    }

    /**
     * Формирует коллекцию значений для запроса последующего запроса
     *
     * @param field       поле выборки
     * @param fieldValues значение ключей
     * @return коллекция значений
     */
    public List<String> getUidList(String indexName, String field, List<FieldValue> fieldValues, long totalCount) {
        List<String> uidList = new ArrayList<>();
        int from = 0;
        do {
            try {
                SearchRequest.Builder request = new SearchRequest.Builder()
                        .index(indexName)
                        .size(REQUEST_PART_SIZE)
                        .from(from);
                if (fieldValues != null && fieldValues.size() > 0){
                    request.query(getTermQuery(field, fieldValues)._toQuery());
                }

                SearchRequest searchRequest = request.build();
                System.out.println(searchRequest.toString());
                SearchResponse<Void> search = client.search(searchRequest, Void.class);
                if (totalCount<=0 && search.hits().total() != null) {
                    totalCount = search.hits().total().value();
                }
                from += REQUEST_PART_SIZE;
                uidList.addAll(search.hits().hits().stream().map(Hit::id).collect(Collectors.toSet()));
            } catch (IOException e) {
                log.error("Ошибка при выполнении запроса в ElasticSearch");
                throw new RuntimeException(e);
            }
        } while (uidList.size() < totalCount);

        return uidList;
    }

    /**
     * Формирует сущность запросов
     *
     * @param field  поле запроса
     * @param values значение выборки (OR)
     * @return сущность запроса
     */
    public TermsQuery getTermQuery(String field, List<FieldValue> values) {
        return TermsQuery.of(ts -> ts
                .field(field)
                .terms(TermsQueryField.of(t -> t.value(values))));
    }
}
