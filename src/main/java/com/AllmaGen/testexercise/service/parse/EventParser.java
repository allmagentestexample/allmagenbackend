package com.AllmaGen.testexercise.service.parse;

import com.AllmaGen.testexercise.model.data.DataIndex;
import com.AllmaGen.testexercise.model.data.Event;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

@RequiredArgsConstructor
public class EventParser implements Parser{

    @Autowired
    private final ElasticsearchRepository eventRepository;

    @Override
    public DataIndex parseLine(String line) throws Exception {
        String[] array = line.split(",");
        if (array.length < Event.FIELD_COUNT) {
            throw new Exception("Строка с ошибкой");
        }
        Event event = new Event();
        event.setUid(array[0]);
        event.setEvent(array[1]);
        return event;
    }

    @Override
    public ElasticsearchRepository getRepository() {
        return eventRepository;
    }


}
