package com.AllmaGen.testexercise.service.parse;

import com.AllmaGen.testexercise.model.data.constant.EventIndexHelper;
import com.AllmaGen.testexercise.model.data.constant.ImpressionIndexHelper;
import com.AllmaGen.testexercise.repository.EventRepository;
import com.AllmaGen.testexercise.repository.ImpressionRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ParserRepositoryFactory {

    private final EventRepository eventRepository;
    private final ImpressionRepository impressionRepository;
    public Parser getParser(String line) throws Exception {
        return switch (line) {
            case EventIndexHelper.FIRST_LINE -> new EventParser(eventRepository);
            case ImpressionIndexHelper.FIRST_LINE -> new ImpressionParser(impressionRepository);
            default -> throw new Exception("Недопустимый формат файла");
        };
    }

    public ElasticsearchRepository getElasticSearchRepository(String line) throws Exception {
        return switch (line) {
            case EventIndexHelper.FIRST_LINE -> eventRepository;
            case ImpressionIndexHelper.FIRST_LINE -> impressionRepository;
            default -> throw new Exception("Недопустимый формат файла");
        };
    }
}
