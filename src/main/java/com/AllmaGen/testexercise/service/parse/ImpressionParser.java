package com.AllmaGen.testexercise.service.parse;

import com.AllmaGen.testexercise.model.data.DataIndex;
import com.AllmaGen.testexercise.model.data.Impression;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import static com.AllmaGen.testexercise.service.parse.ParseHelper.convertStringToDataElastic;

@RequiredArgsConstructor
public class ImpressionParser implements Parser{
    private final Logger log = LogManager.getLogger(this.getClass());
    private final ElasticsearchRepository impressionRepository;

    @Override
    public DataIndex parseLine(String line) throws Exception {
        String[] array = line.split(",");
        if (array.length < Impression.FIELD_COUNT) {
            log.error("Строка {} не соответствует формату", line);
            throw new Exception("Строка с ошибкой");
        }
        Impression impression = new Impression();
        impression.setRegTime(convertStringToDataElastic(array[0]));
        impression.setUid(array[1]);
        try {
            impression.setFcImpChk(java.lang.Byte.parseByte(array[2]));
        } catch (RuntimeException e) {
            log.warn("Не возможно преобразовать {} в число", array[2]);
            throw new Exception();
        }

        try {
            impression.setFcTimeChk(java.lang.Byte.parseByte(array[3]));
        } catch (RuntimeException e) {
            log.warn("Не возможно преобразовать {} в число", array[3]);
            throw new Exception();
        }

        try {
            impression.setUtmtr(java.lang.Byte.parseByte(array[4]));
        } catch (RuntimeException e) {
            log.warn("Не возможно преобразовать {} в число", array[4]);
            throw new Exception();
        }

        try {
            impression.setMmDma(java.lang.Integer.valueOf(array[5]));
        } catch (RuntimeException e) {
            log.warn("Не возможно преобразовать {} в число", array[5]);
            throw new Exception();
        }

        impression.setOsName(array[6]);
        impression.setModel(array[7]);
        impression.setHardware(array[8]);
        impression.setSiteId(array[9]);

        return impression;
    }

    @Override
    public ElasticsearchRepository getRepository() {
        return impressionRepository;
    }


}
