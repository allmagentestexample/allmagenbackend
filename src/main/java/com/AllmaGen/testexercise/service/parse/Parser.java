package com.AllmaGen.testexercise.service.parse;

import com.AllmaGen.testexercise.model.data.DataIndex;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface Parser {

    public DataIndex parseLine(String line) throws Exception;

    public ElasticsearchRepository getRepository();
}
