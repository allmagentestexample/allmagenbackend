package com.AllmaGen.testexercise.model.statistic;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class EventRecord {
    @JsonProperty("event_title")
    private final String event;

    @JsonProperty("count")
    private final long count;

    @JsonProperty("percent")
    private double percent;
}
