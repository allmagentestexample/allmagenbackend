package com.AllmaGen.testexercise.model.statistic;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CountResponse {

    @JsonProperty("event_count")
    private long eventCount;

    @JsonProperty("impression_count")
    private long impressionCount;

}
