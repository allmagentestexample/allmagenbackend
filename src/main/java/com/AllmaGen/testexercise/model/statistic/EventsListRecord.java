package com.AllmaGen.testexercise.model.statistic;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class EventsListRecord {
    @JsonProperty("record")
    private List<EventRecord> eventRecords;

    public EventsListRecord(int size) {
        eventRecords = new ArrayList<>(size);
    }

    public void addEvent(EventRecord eventRecord){
        eventRecords.add(eventRecord);
    }
}
