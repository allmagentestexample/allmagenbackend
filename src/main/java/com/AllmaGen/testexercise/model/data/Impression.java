package com.AllmaGen.testexercise.model.data;

import com.AllmaGen.testexercise.model.data.constant.ImpressionIndexHelper;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

import static org.springframework.data.elasticsearch.annotations.FieldType.*;


@Data
@Document(indexName = "impression")
public class Impression implements DataIndex {
    @Transient
    public final static int FIELD_COUNT = 10;

    @Id
    @Field(name = "uid", type = Keyword)
    private String uid;

    @Field(name = "reg_time", type = Date)
    private String regTime;

    @Field(name = "fc_imp_chk", type = Byte)
    private byte fcImpChk;

    @Field(name = "fc_time_chk", type = Byte)
    private byte fcTimeChk;

    @Field(name = "utmtr", type = Byte)
    private byte utmtr;

    @Field(name = "mm_dma", type = Keyword)
    private Integer mmDma;

    @Field(name = "osName", type = Keyword)
    private String osName;

    @Field(name = "model", type = Keyword)
    private String model;

    @Field(name = "hardware", type = Keyword)
    private String hardware;

    @Field(name = "site_id", type = Keyword)
    private String siteId;


}
