package com.AllmaGen.testexercise.model.data.constant;

public class EventIndexHelper {
    public static final String INDEX_NAME = "event";

    public static final String FIELD_UID = "uid";
    public static final String FIELD_EVENT = "event_title";

    public static final String FIRST_LINE = "uid,tag";

}
