package com.AllmaGen.testexercise.model.data;

import com.AllmaGen.testexercise.model.data.constant.EventIndexHelper;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

import static org.springframework.data.elasticsearch.annotations.FieldType.Keyword;

@Data
@Document(indexName = "event")
public class Event implements DataIndex {

    @Transient
    public final static int FIELD_COUNT = 2;

    @Id
    @Field(name = "uid", type = Keyword)
    private String uid;

    @Field(name = "event_title", type = Keyword)
    private String event;

}
