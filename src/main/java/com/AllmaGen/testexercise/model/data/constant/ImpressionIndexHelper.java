package com.AllmaGen.testexercise.model.data.constant;

public class ImpressionIndexHelper {
    public final static String INDEX_NAME = "impression";

    public final static String FIELD_DATA = "reg_time";
    public final static String FIELD_UID = "uid";

    public static final String FIRST_LINE = "reg_time,uid,fc_imp_chk,fc_time_chk,utmtr,mm_dma,osName,model,hardware,site_id";
}
