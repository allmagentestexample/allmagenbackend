package com.AllmaGen.testexercise.model.ctr;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class AxisResponse {
    private final List<Object> xAxis;
    private final List<AxisYValues> yAxis;
    public AxisResponse(List<Object> xAxis){
        this.xAxis = xAxis;
        this.yAxis = new ArrayList<>();
    }

    public void addYaxis(String dName, List<Object> values){
        yAxis.add(new AxisYValues(values, dName));
    }

    @Data
    @AllArgsConstructor
    public class AxisYValues {
        @JsonProperty("data")
        private List<Object> yAxis;

        @JsonProperty("name")
        private String title;
    }
}


