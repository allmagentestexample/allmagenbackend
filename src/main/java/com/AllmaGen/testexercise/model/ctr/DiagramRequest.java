package com.AllmaGen.testexercise.model.ctr;

import com.AllmaGen.testexercise.model.PeriodEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
public class DiagramRequest {
    @JsonProperty("period")
    private PeriodEnum period;

    @JsonProperty("events")
    private List<String> events;
}
