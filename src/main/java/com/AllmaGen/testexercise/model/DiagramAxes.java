package com.AllmaGen.testexercise.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Data
public class DiagramAxes {

    Map<String, Map<String, Long>> valuesMap;
    private List<Object> xAxis;


    public void setSize(int size) {
        xAxis = new ArrayList<>(size);
        valuesMap = new HashMap<>(size);
    }

    public void axisValueMap(String date, Long value) {
        xAxis.add(date);
        Map<String, Long> map = new HashMap<>();
        map.put("all", value);
        valuesMap.put(date, map);
    }

    public void putValueMap(String data, String event, Long value) {
        Map<String, Long> map = valuesMap.get(data);
        map.put(event, value);
    }


}

