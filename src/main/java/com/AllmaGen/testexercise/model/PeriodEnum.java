package com.AllmaGen.testexercise.model;

public enum PeriodEnum {
    DAILY,
    WEEK,
    MONTH
}
