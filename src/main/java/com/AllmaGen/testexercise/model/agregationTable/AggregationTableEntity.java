package com.AllmaGen.testexercise.model.agregationTable;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Map;

@Data
@RequiredArgsConstructor
public class AggregationTableEntity {
    private final String name;
    private final long countImpression;
    private Map<String, Long> events;

    private long fclickCount;
}
