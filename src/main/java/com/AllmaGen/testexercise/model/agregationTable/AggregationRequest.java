package com.AllmaGen.testexercise.model.agregationTable;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class AggregationRequest {

    @JsonProperty("parameter")
    private String paramName;
}
