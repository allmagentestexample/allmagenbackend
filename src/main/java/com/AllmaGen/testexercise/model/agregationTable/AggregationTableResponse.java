package com.AllmaGen.testexercise.model.agregationTable;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
public class AggregationTableResponse {

    @JsonProperty("list_event")
    private List<String> eventList;

    private final List<Row> table;

    public AggregationTableResponse(int size){
        table = new ArrayList<>(size);
    }

    public void addRow(String name, long impressions, Double ctr, Map<String, Double> eventMap){
        table.add(new Row(name, impressions, ctr, eventMap));
    }

    @Data
    @AllArgsConstructor
    public static class Row{
        @JsonProperty("name")
        private String name;

        @JsonProperty("impressions")
        private long impressions;

        @JsonProperty("ctr")
        private Double ctr;

        @JsonProperty("evpm")
        private Map<String, Double> eventMap;
    }
}
