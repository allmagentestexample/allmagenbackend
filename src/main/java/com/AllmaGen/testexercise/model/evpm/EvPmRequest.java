package com.AllmaGen.testexercise.model.evpm;

import com.AllmaGen.testexercise.model.PeriodEnum;

import java.util.List;

public class EvPmRequest {
    PeriodEnum period;
    List<String> events;
}
