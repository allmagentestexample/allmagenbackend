package com.AllmaGen.testexercise.controller;

import com.AllmaGen.testexercise.model.agregationTable.AggregationRequest;
import com.AllmaGen.testexercise.model.agregationTable.AggregationTableResponse;
import com.AllmaGen.testexercise.model.ctr.DiagramRequest;
import com.AllmaGen.testexercise.model.ctr.AxisResponse;
import com.AllmaGen.testexercise.model.statistic.CountResponse;
import com.AllmaGen.testexercise.model.statistic.EventsListRecord;
import com.AllmaGen.testexercise.service.StatisticService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/")
@RequiredArgsConstructor
public class StatisticController {
    private final Logger log = LogManager.getLogger(this.getClass());
    private final StatisticService statisticService;


    @GetMapping(value = "/get_count/")
    @Operation(summary = "Общее количество событий и просмотров", tags = "Получение общего количества событий и просмотров за все время")
    public CountResponse getCountStatistic(){
        log.info("Запрос общего количества событий и просмотров за все время");
        return statisticService.getCountResponse();
    }

    @GetMapping(value = "/get_event_list/")
    @Operation(summary = "Список событий и их количество", tags = "Получение списка событий и их количества")
    public EventsListRecord getEventListStatistic(){
        log.info("Запрос списка событий и их количества");
        return statisticService.getEventListRecord();
    }

    @PostMapping(value = "/get_ctr/")
    @Operation(summary = "Метрика CTR", tags = "Диаграмма CTR по времени с заданным диапазоном")
    public AxisResponse getCtr(@RequestBody @Parameter(description = "Параметры расчета графика") DiagramRequest diagramRequest){
        log.info("Диаграмма CTR по времени с заданным диапазоном");
        return statisticService.getCtr(diagramRequest);
    }

    @PostMapping(value = "/get_evpm/")
    @Operation(summary = "Метрика EVPM", tags = "Диаграмма EVPM по времени с заданным диапазоном и с указанными событиями")
    public AxisResponse getEvPm(@RequestBody @Parameter(description = "Параметры расчета графика") DiagramRequest diagramRequest){
        return statisticService.getEvPm(diagramRequest);
    }

    @PostMapping(value = "/get_table/")
    @Operation(summary = "Агрегационная таблица", tags = "Формирование агрегациооной таблицы по заданному параметру")
    public AggregationTableResponse getTable(@RequestBody AggregationRequest param){
        return statisticService.getTable(param);
    }

}
