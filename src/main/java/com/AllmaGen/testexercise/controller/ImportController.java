package com.AllmaGen.testexercise.controller;

import com.AllmaGen.testexercise.service.ElasticSearchService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin
@RestController
@RequestMapping("/api/")
@RequiredArgsConstructor
public class ImportController {
    private final Logger log = LogManager.getLogger(this.getClass());

    private final ElasticSearchService elasticSearchService;

    @PostMapping(value = "/import/")
    @Operation(summary = "Импорт данных", tags = "Загрузка файла с событиями или просмотрами")
    public void importFile(@RequestPart("file") @Parameter(description = "Файл") MultipartFile file) {
        elasticSearchService.addData(file);
    }

}
